/* SPDX-License-Identifier: GPL-2.0-only */
#ifndef __MISC_ASUS_EC_H
#define __MISC_ASUS_EC_H

#include <linux/notifier.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>

struct i2c_client;

struct asusec_info {
	const char			*model;
	const char			*name;
	struct i2c_client		*dockram;
	struct workqueue_struct		*wq;
	struct blocking_notifier_head	 notify_list;
};

struct asusec_platform_data {
	u8 batt_addr;
	u8 ctrl_addr;
};

#define DOCKRAM_ENTRIES			0x100
#define DOCKRAM_ENTRY_SIZE		32
#define DOCKRAM_ENTRY_BUFSIZE		(DOCKRAM_ENTRY_SIZE + 1)

/* interrupt sources */
#define ASUSEC_OBF_MASK			BIT(0)
#define ASUSEC_KEY_MASK			BIT(2)
#define ASUSEC_KBC_MASK			BIT(3)
#define ASUSEC_AUX_MASK			BIT(5)
#define ASUSEC_SCI_MASK			BIT(6)
#define ASUSEC_SMI_MASK			BIT(7)

/* SMI notification codes */
#define ASUSEC_SMI_POWER_NOTIFY		0x31    /* [un]plugging USB cable */
#define ASUSEC_SMI_HANDSHAKE		0x50    /* response to ec_req edge */
#define ASUSEC_SMI_WAKE			0x53
#define ASUSEC_SMI_RESET		0x5F
#define ASUSEC_SMI_ADAPTER_EVENT	0x60    /* [un]plugging charger to dock */
#define ASUSEC_SMI_BACKLIGHT_ON		0x63    // dock
#define ASUSEC_SMI_AUDIO_DOCK_IN	0x70    // dock

#define ASUSEC_SMI_ACTION(code)		(ASUSEC_SMI_MASK | ASUSEC_OBF_MASK | (ASUSEC_SMI_##code << 8))

/* control register [0x0A] layout */
#define ASUSEC_CTL_SIZE			8

/*
 * EC reports power from 40-pin connector in the LSB of the control
 * register.  The following values have been observed (xor 0x02):
 *
 * PAD-ec no-plug  0x40 / PAD-ec DOCK     0x20 / DOCK-ec no-plug 0x40
 * PAD-ec AC       0x25 / PAD-ec DOCK+AC  0x24 / DOCK-ec AC      0x25
 * PAD-ec USB      0x45 / PAD-ec DOCK+USB 0x24 / DOCK-ec USB     0x41
 */

// TODO: Double chech these values since prev values were
//#define ASUSEC_CTL_SUSB_MODE		BIT_ULL(9)
//#define ASUSEC_CTL_FACTORY_MODE	BIT_ULL(38)
#define ASUSEC_CTL_DIRECT_POWER_SOURCE	BIT_ULL(0)	// 0.0
#define ASUSEC_STAT_CHARGING		BIT_ULL(2)	// 0.2
#define ASUSEC_CTL_FULL_POWER_SOURCE	BIT_ULL(5)	// 0.5 @ DEC ac_over_10v
#define ASUSEC_CTL_SUSB_MODE		BIT_ULL(11)	// 1.1
#define ASUSEC_CMD_SUSPEND_S3		BIT_ULL(41)	// 4.1 @ PEC enter_s3
#define ASUSEC_CTL_TEST_DISCHARGE	BIT_ULL(43)	// 4.3
#define ASUSEC_CMD_SUSPEND_INHIBIT	BIT_ULL(45)	// 4.5 @ DEC suspend [XXX bug? 0x20 clear+set]
#define ASUSEC_CTL_FACTORY_MODE		BIT_ULL(46)	// 4.6
#define ASUSEC_CTL_KEEP_AWAKE		BIT_ULL(47)	// 4.7 @ DEC ec_wake ["keep EC awake when PAD in LP0"]
#define ASUSEC_CTL_USB_CHARGE		BIT_ULL(50)	// 5.0 @ aw8ec, DEC: bug? [4] <- [5] | 0x1
#define ASUSEC_CMD_SWITCH_HDMI		BIT_ULL(70)	// 7.0
#define ASUSEC_CMD_WIN_SHUTDOWN		BIT_ULL(76)	// 7.6

#define ASUSEC_DOCKRAM_INFO_MODEL	0x01
#define ASUSEC_DOCKRAM_INFO_FW		0x02
#define ASUSEC_DOCKRAM_INFO_CFGFMT	0x03
#define ASUSEC_DOCKRAM_INFO_HW		0x04
#define ASUSEC_DOCKRAM_CONTROL		0x0A
#define ASUSEC_DOCKRAM_BATT_CTL		0x14

/* dockram comm */
int asus_dockram_read(struct i2c_client *client, int reg, char *buf);
int asus_dockram_write(struct i2c_client *client, int reg, const char *buf);
int asus_dockram_access_ctl(struct i2c_client *client,
			    u64 *out, u64 mask, u64 xor);
struct i2c_client *devm_asus_dockram_get(struct device *parent);

/* EC public API */
static inline struct asusec_info *asusec_cell_to_ec(struct platform_device *pdev)
{
	return dev_get_drvdata(pdev->dev.parent);
}

static inline int asusec_get_ctl(const struct asusec_info *ec, u64 *out)
{
	return asus_dockram_access_ctl(ec->dockram, out, 0, 0);
}

static inline int asusec_update_ctl(const struct asusec_info *ec,
				    u64 mask, u64 xor)
{
	return asus_dockram_access_ctl(ec->dockram, NULL, mask, xor);
}

static inline int asusec_set_ctl_bits(const struct asusec_info *ec, u64 mask)
{
	return asus_dockram_access_ctl(ec->dockram, NULL, mask, mask);
}

static inline int asusec_clear_ctl_bits(const struct asusec_info *ec, u64 mask)
{
	return asus_dockram_access_ctl(ec->dockram, NULL, mask, 0);
}

int asusec_i2c_command(const struct asusec_info *ec, u16 data);
int devm_asusec_register_notifier(struct platform_device *dev,
				  struct notifier_block *nb);

#endif /* __MISC_ASUS_EC_H */
